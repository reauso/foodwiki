# Foodwicki

GruppenID: 4

Gruppenmitglieder: Rene Ebertowski, Dennis Amthor, Jan Paulsen 

## Inhalt
1. Projekt einrichten
2. Daten aufbereiten
3. Netz trainieren und testen
4. Netz nutzen

### 1 Projekt einrichten

Dieses Projekt wurde mit Poetry zum Verwalten von pythons virtual environments angelegt.
Der .venv-Ordner von Python liegt nicht mit im Git-Repository, lediglich eine pyproject.toml wird dort abgelegt.
Diese Datei listet alle Abhängigkeiten auf.
Um diese Abhängigkeiten zu installieren, muss poetry vorher einmalig eingerichtet und konfiguriert werden. 

Install Poetry:
```
pip install poetry
```

Nun muss eingestellt werden, dass Poetry die .venv im zugehörigen Projekt anlegt und nicht im globalen Python-Verzeichnis.
```
poetry config virtualenvs.in-project true
```

Um die Abhängigkeiten unseres Projekts lokal herunterzuladen und die .venv auf Basis der pyproject.toml zu erstellen, muss folgender Befehl ausgeführt werden. Wichtig, vorher in den Project Ordner navigieren, indem sich die pyporject.toml befindet. Dann:
```
poetry update
```

Das Projekt ist jetzt ordentlich erstellt.

### 2 Daten aufbereiten

Zum Trainieren werden Ordner mit darin liegenden Bildern benötigt. Der Ordnername bildet das Label und die darin liegenden Bilder die Trainigsdaten des jeweiligen Labels. In unserem Beispiel haben wir das Netz mit 5 Klassen a 500 Bildern trainiert.
Das Einlesen und Trainieren geschieht mithilfe von Datasets, um RAM zu sparen.
Mittels der Library Pillow werden die Bilder auf 384x384 RGB skaliert. Um trainierte Kategorien zu speichern, wird eine extra "Category.txt"-Datei angelegt. Diese wird im späteren Verlauf weiterverwendet.

### 3 Netz trainieren/testen

Durch das training.py-Skript können der Trainings- und der Testprozess des Netzes anhand vorher erstellter Datensätze ausgeführt werden.
Unsere Testdatensätze bestehen aus 5 Klassen mit je 50 Bildern zum Trainieren und einem Ordner mit jeweiligen Testbildern zum Validieren.
Die Prozesse werden direkt hintereinander ausgeführt und die Ergebnisse durch eine Konsolenausgabe dokumentiert.

Beschreibung des Models(stand 09.07.2020)
```
Model: "FoodWiki"
_________________________________________________________________
Layer (type)                 Output Shape              Param #
=================================================================
input_1 (InputLayer)         [(None, 384, 384, 3)]     0
_________________________________________________________________
conv2d (Conv2D)              (None, 378, 378, 32)      4736
_________________________________________________________________
spatial_dropout2d (SpatialDr (None, 378, 378, 32)      0
_________________________________________________________________
max_pooling2d (MaxPooling2D) (None, 75, 75, 32)        0
_________________________________________________________________
dropout (Dropout)            (None, 75, 75, 32)        0
_________________________________________________________________
conv2d_1 (Conv2D)            (None, 69, 69, 20)        31380
_________________________________________________________________
spatial_dropout2d_1 (Spatial (None, 69, 69, 20)        0
_________________________________________________________________
max_pooling2d_1 (MaxPooling2 (None, 9, 9, 20)          0
_________________________________________________________________
dropout_1 (Dropout)          (None, 9, 9, 20)          0
_________________________________________________________________
flatten (Flatten)            (None, 1620)              0
_________________________________________________________________
dense (Dense)                (None, 64)                103744
_________________________________________________________________
dropout_2 (Dropout)          (None, 64)                0
_________________________________________________________________
dense_1 (Dense)              (None, 5)                 325
=================================================================
Total params: 140,185
Trainable params: 140,185
```

Training/Evaluation des Models(stand 11.06.2020):
```
49/63 [======================>.......] - ETA: 0s - loss: 2.1678 - accuracy: 0.2883
Epoch 00001: saving model to training_2/cp-0001.ckpt
63/63 [==============================] - 3s 56ms/step - loss: 2.1662 - accuracy: 0.2910
Epoch 2/10
35/63 [===============>..............] - ETA: 1s - loss: 2.1576 - accuracy: 0.3036
Epoch 00002: saving model to training_2/cp-0002.ckpt
63/63 [==============================] - 3s 45ms/step - loss: 2.1611 - accuracy: 0.3000
Epoch 3/10
23/63 [=========>....................] - ETA: 1s - loss: 2.1432 - accuracy: 0.3179
Epoch 00003: saving model to training_2/cp-0003.ckpt
63/63 [==============================] - 3s 43ms/step - loss: 2.1611 - accuracy: 0.3000
Epoch 4/10
 9/63 [===>..........................] - ETA: 2s - loss: 2.1487 - accuracy: 0.3125
Epoch 00004: saving model to training_2/cp-0004.ckpt
59/63 [===========================>..] - ETA: 0s - loss: 2.1592 - accuracy: 0.3019
Epoch 00004: saving model to training_2/cp-0004.ckpt
63/63 [==============================] - 3s 51ms/step - loss: 2.1611 - accuracy: 0.3000
Epoch 5/10
47/63 [=====================>........] - ETA: 0s - loss: 2.1779 - accuracy: 0.2832
Epoch 00005: saving model to training_2/cp-0005.ckpt
63/63 [==============================] - 3s 47ms/step - loss: 2.1611 - accuracy: 0.3000
Epoch 6/10
33/63 [==============>...............] - ETA: 1s - loss: 2.1657 - accuracy: 0.2955
Epoch 00006: saving model to training_2/cp-0006.ckpt
63/63 [==============================] - 3s 45ms/step - loss: 2.1611 - accuracy: 0.3000
Epoch 7/10
21/63 [=========>....................] - ETA: 1s - loss: 2.1903 - accuracy: 0.2708
Epoch 00007: saving model to training_2/cp-0007.ckpt
63/63 [==============================] - 3s 44ms/step - loss: 2.1611 - accuracy: 0.3000
Epoch 8/10
 7/63 [==>...........................] - ETA: 1s - loss: 2.1932 - accuracy: 0.2679
Epoch 00008: saving model to training_2/cp-0008.ckpt
57/63 [==========================>...] - ETA: 0s - loss: 2.1684 - accuracy: 0.2928
Epoch 00008: saving model to training_2/cp-0008.ckpt
63/63 [==============================] - 3s 47ms/step - loss: 2.1611 - accuracy: 0.3000
Epoch 9/10
45/63 [====================>.........] - ETA: 0s - loss: 2.1681 - accuracy: 0.2931
Epoch 00009: saving model to training_2/cp-0009.ckpt
63/63 [==============================] - 3s 43ms/step - loss: 2.1611 - accuracy: 0.3000
Epoch 10/10
31/63 [=============>................] - ETA: 1s - loss: 2.1668 - accuracy: 0.2944
Epoch 00010: saving model to training_2/cp-0010.ckpt
63/63 [==============================] - 3s 42ms/step - loss: 2.1611 - accuracy: 0.3000
-----------------------
evaluate neural network
4/4 [==============================] - 0s 38ms/step - loss: 2.1612 - accuracy: 0.3000
Test loss: 2.161151647567749
Test accuracy: 0.30000001192092896
```

### 4 Netz nutzen

Für das Nutzen der Anwendung haben wir uns für das erste Model des hier gezeigten RMS-Optimizers entschieden.

![Agents_Accuracy](/doku/Agents Accuracy.PNG)
*Ausschnitt Tensorboard: die gezeigte Accurracy bezieht sich auf die validation data*

Um die Klassifizierungs-Anwendung des Netzes zu starten, wird das app.py-Skript verwendet. Wie in der Konsolenausgabe 
```Please insert the path to a picture you want to be analyzed:```
angezeigt, muss der Pfad zu einem Bild angegeben werden, welches klassifiziert werden soll.

Bevor das Ergebnis angezeigt wird, zeigt eine Ausgabe welche Gerichte das Netz erkennen kann. In unserem Beispiel sind dies ```chocolate_cake, edamame, french_fries, greek_salad, red_velvet_cake```.

Auf Basis unseres ausgewählten Checkpoints des Trainingsergebnisses bewertet das Netz nun das eingegebene Bild und kategorisiert es. 
```Your picture shows the following food: Fries``` 
Damit zur richtigen Klasse der passende Kategoriename gefunden wird, verwendet das Skript die im vorherigen Schritt abgespeicherte "Category.txt"-Datei.


