import analyze_image


def app():
    print("Welcome to Foodwicki!")
    print("____________________")
    print("We automatically analyze your picture to recognize food.")
    path = input(
        "Please insert the path to a picture you want to be analyzed:"
    )
    result = analyze_image.analyze(path)

    # match found class to the right category
    with open("./categories_trained.txt", "r") as f:  # MY COWORKERS USE "../categories_trained.txt"
        content = f.read()
    categories = content.split(",")[:-1]
    # select right result
    result_category = categories[int(result)]

    print("trained categories: " + str(categories))

    print("Your picture shows the following food: " + str(result_category))


if __name__ == "__main__":
    app()
