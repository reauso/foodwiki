import numpy as np
from tensorflow import config
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow import train
from tensorflow.keras.callbacks import TensorBoard
import time
from os import path
import tensorflow as tf
import logging


class NeuralNet:
    """
    The neural net class to perform neural net tasks.
    """

    def __init__(self, log_level=logging.DEBUG):
        """
        Initializes the neural net instance by defining the model of the neural net and compiling it.
        Additionally the callback to save the model during training is created.
        """

        tf.get_logger().setLevel(log_level)
        debug_gpu()

        # create neural net layers
        inputs, outputs, = self.create_layers()

        # create Model
        self.create_model(inputs, outputs)

        self.initialize_callbacks(int(time.time()))

    def initialize_callbacks(self, id):
        name = "foodwicki_agent_RMS_{}".format(id)
        self.tensor_board_callback = TensorBoard(
            log_dir="logs/{}".format(name)
        )
        # create a checkpoint dir, so weights are saved out and get picked up when keep on training
        checkpoint_path = "training/cp-{}".format(id) + ".ckpt"
        self.checkpoint_dir = path.dirname(checkpoint_path)
        self.check_point_callback = keras.callbacks.ModelCheckpoint(
            filepath=checkpoint_path,
            verbose=1,
            save_best_only=True,
            save_weights_only=True,
            save_freq="epoch",
        )

    def create_layers(self):
        """
        Creates all layers of the network.
        :return: The inputs and outputs to compile the model.
        """
        input = keras.Input(shape=(384, 384, 3))
        features = layers.Conv2D(32, (7, 7), activation="relu")(input)
        features = layers.SpatialDropout2D(0.1)(features)
        features = layers.MaxPooling2D((5, 5))(features)
        features = layers.Dropout(0.1)(features)
        features = layers.Conv2D(20, (7, 7), activation="relu")(features)
        features = layers.SpatialDropout2D(0.1)(features)
        features = layers.MaxPooling2D((7, 7))(features)
        features = layers.Dropout(0.1)(features)
        x = layers.Flatten()(features)
        x = layers.Dense(64, activation="relu")(x)
        x = layers.Dropout(0.25)(x)
        output = layers.Dense(5, activation="softmax")(x)
        return input, output

    def create_model(self, inputs, outputs):
        """
        Creates the model of the neural network.
        :param inputs: The inputs of the neural net model.
        :param outputs: The outputs of the neural net model.
        """
        self.model = keras.Model(inputs=inputs, outputs=outputs, name="FoodWiki")
        # self.model.summary()

        self.history = None

        self.model.compile(
            loss=keras.losses.SparseCategoricalCrossentropy(from_logits=True),
            optimizer=keras.optimizers.RMSprop(),
            # optimizer=keras.optimizers.Adam(),
            metrics=["accuracy"],
        )

    def train(self, train_data, validation_data=None, initial_epoch=0):
        """
        Trains the neural network with the given data and labels.
        :param train_data: The train data. This is the image data itself.
        """
        iterations = 5
        epochs_per_iterations = 40;
        for i in range(iterations):
            print("iteration {}".format(i))
            self.history = self.model.fit(
                train_data,
                validation_data=validation_data,
                initial_epoch=i * epochs_per_iterations + initial_epoch,
                epochs=(i + 1) * epochs_per_iterations + initial_epoch,
                callbacks=[self.check_point_callback, self.tensor_board_callback],
            )

            lastAccuracies = self.history.history["val_accuracy"][-5: -1]
            if sum(lastAccuracies) / len(lastAccuracies) < 0.3:
                print("model is too bad")
                break

            print("continue learning")

    def evaluate(self, test_data, test_labels):
        """
        Evaluates the neural net by passing test data to the net and compare the result with the given test labels.
        :param test_data: The test data for the evaluation.
        :param test_labels: The labels of the test data. Note that the index of test_data and the associated label has to be the same.
        """
        test_scores = self.model.evaluate(test_data, test_labels)
        print("Test loss:", test_scores[0])
        print("Test accuracy:", test_scores[1])

    def predict(self, data):
        """
        Applies the neural net to the given data.
        :param data: The image data to predict.
        :return: The predicted labels of the input data.
        """
        prediction = self.model.predict(data)
        return np.argmax(prediction, axis=1)

    def save_model(self, file_path):
        """
        Saves the weights and architecture of the neural net.
        :param file_path: The path where to save the model.
        """
        self.model.save(file_path)

    def load_latest_checkpoint(self, checkpoint_dir=None):
        """
        Loads the latest checkpoint of past training sessions.
        :param checkpoint_dir: The directory where the checkpoints are saved.
        if no param is given, it uses the default path
        """
        if checkpoint_dir != None:
            latest = train.latest_checkpoint(checkpoint_dir)
        else:
            latest = train.latest_checkpoint(self.checkpoint_dir)

        inputs, outputs, = self.create_layers()

        # create Model
        self.create_model(inputs, outputs)

        # Load the previously saved weights
        self.model.load_weights(latest)

    def load_checkpoint(self, id, checkpoint_dir=None):
        """
        Loads a specific checkpoint
        :param id; checkpoint id
        :param checkpoint_dir: The directory where the checkpoints are saved.
        if no param is given, it uses the default path
        """
        inputs, outputs, = self.create_layers()

        # create Model
        self.create_model(inputs, outputs)

        # Load the previously saved weights
        if checkpoint_dir != None:
            self.model.load_weights(checkpoint_dir + "/cp-" + id + ".ckpt")
        else:
            self.model.load_weights(self.checkpoint_dir + "/cp-" + id + ".ckpt")


def debug_gpu():
    gpus = config.experimental.list_physical_devices("GPU")
    if gpus:
        try:
            # Currently, memory growth needs to be the same across GPUs
            for gpu in gpus:
                config.experimental.set_memory_growth(gpu, True)
            logical_gpus = config.experimental.list_logical_devices("GPU")
            print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
        except RuntimeError as e:
            # Memory growth must be set before GPUs have been initialized
            print(e)
