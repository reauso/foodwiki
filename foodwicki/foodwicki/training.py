import numpy as np
import tensorflow as tf
import os
import pathlib

import neuralnet.NeuralNetInteractor as nn


def get_label(file_path):
    """
    Retrieve the label of an image by taking the parent folder's name
    :param file_path: path to an image
    :return: label as string
    """
    # convert the path to a list of path components
    parts = tf.strings.split(file_path, os.path.sep)
    # The second to last is the class-directory
    return tf.where(parts[-2] == classes)[0]


def decode_img(img):
    """
    Decode an image to jpg, normalize pixel values and resize to model input shape
    :param img: image file
    :return: pixel data
    """
    # convert the compressed string to a 3D uint8 tensor
    img = tf.image.decode_jpeg(img, channels=3)
    # Use `convert_image_dtype` to convert to floats in the [0,1] range.
    img = tf.image.convert_image_dtype(img, tf.float32)
    # resize the image to the desired size.
    return tf.image.resize(img, [384, 384])


def process_path(file_path):
    """
    Process a file for passing it to the model
    :param file_path: path to an image
    :return: pixel data, label as string
    """
    label = get_label(file_path)
    # load the raw data from the file as a string
    img = tf.io.read_file(file_path)
    img = decode_img(img)
    return img, label


def store_categories(classes):
    """
    stores the found categories inside a .txt.
    This is used when the NN predicts to match the found class with the stored category
    """
    outputString = ""
    for classi in classes:
        outputString += (classi + ",")

    f = open("./categories_trained.txt", "w")  # MY COWORKERS USE "../categories_trained.txt" as PATH
    f.write(outputString)
    f.close()


if __name__ == "__main__":

    nn.debug_gpu()

    ################ DATA IMPORT AND PREPERATION ##############

    # TRAIN DATABASE

    data_dir = pathlib.Path("../../data/images/train")
    test_dir = pathlib.Path("../../data/images/test")

    classes = np.array(
        [item.name for item in data_dir.glob("*") if item.name != "LICENSE.txt"]
    )

    print(classes)

    # write classes to TXT
    store_categories(classes)

    train_ds = tf.data.Dataset.list_files(str(data_dir / "*/*.jpg"))
    test_ds = tf.data.Dataset.list_files(str(test_dir / "*/*.jpg"))

    labeled_train_ds = train_ds.map(process_path)
    labeled_test_ds = test_ds.map(process_path)

    labeled_train_ds = labeled_train_ds.batch(5).repeat(1)
    labeled_test_ds = labeled_test_ds.batch(5).repeat(1)

    ############# NEURONAL NET ##############

    iteration_count = 10
    for i in range(iteration_count):
        neural_net = nn.NeuralNet()
        # neural_net.load_checkpoint("1593899650")

        # NETWORK IS TRAINED
        print("--------------------")
        print("train neural network")
        neural_net.train(labeled_train_ds, labeled_test_ds, initial_epoch=0)
