from neuralnet import NeuralNetInteractor as nn
from PIL import Image
import numpy as np
import logging


def analyze(test_dir="../data/test/2246161.jpg"):
    """
    Analyze a picture by using the neural net
    :param test_dir: path to image file
    uses sample image if no path given
    :return: the predicted class (index)
    """
    # analyze image
    image_data = read_image(test_dir)

    # put into NN
    neural_net = nn.NeuralNet(log_level=logging.ERROR)
    # select last training output
    neural_net.load_checkpoint("1594147727")
    result = neural_net.predict(image_data)

    return result


def read_image(path: str):
    """
    Read the pixel data of an image
    Automatic resizing it to the needed model input shape
    :param path: path to image file
    :return: pixel data as numpy array
    """
    images_rgb = []
    image = Image.open(path)
    # resize
    reduced_image = image.resize((384, 384), Image.ANTIALIAS)
    # get RGB
    reduced_image_rgb = reduced_image.convert("RGB")
    # to numpy
    images_rgb.append(np.asarray(reduced_image_rgb))
    images_rgb_np = np.array(images_rgb)

    return images_rgb_np


if __name__ == "__main__":
    analyze()
