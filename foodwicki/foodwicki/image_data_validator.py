import numpy as np
import tensorflow as tf
import os
import pathlib


def get_label(file_path):
    # convert the path to a list of path components
    parts = tf.strings.split(file_path, os.path.sep)
    # The second to last is the class-directory
    return tf.where(parts[-2] == classes)[0]


def decode_img(img):
    # convert the compressed string to a 3D uint8 tensor
    img = tf.image.decode_jpeg(img, channels=3)
    # Use `convert_image_dtype` to convert to floats in the [0,1] range.
    img = tf.image.convert_image_dtype(img, tf.float32)
    # resize the image to the desired size.
    return tf.image.resize(img, [384, 384])


def process_path(file_path):
    label = get_label(file_path)
    # load the raw data from the file as a string
    img = tf.io.read_file(file_path)
    img = decode_img(img)
    return img, label


if __name__ == "__main__":

    data_dir = pathlib.Path("../../data/images/train")
    classes = np.array(
        [item.name for item in data_dir.glob("*") if item.name != "LICENSE.txt"]
    )

    print(classes)

    train_ds = tf.data.Dataset.list_files(str(data_dir / "*/*.jpg"))

    counter = 0

    for file_path in train_ds:
        print(counter)
        try:
            process_path(file_path)
        except:
            print(file_path)
        counter += 1

    print("finish")
